import random
import math
for timeOfDay in ['morning', 'afternoon', 'evening']:
    print('Good ' + timeOfDay + '!')
    print('How are you feeling?')
    feeling = input()
    print('I am happy to hear that you are feeling ' + feeling + '.')


def func(x=1, y=2):
    def f(z):
        return x + y + z
    return f
f = func()
print(f(4))

print(math.e)
 